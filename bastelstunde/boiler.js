const Raspi = require('raspi-io')
const five = require('johnny-five')
const Stepper=require('./stepper').Stepper
const mqtt=require('mqtt').connect('mqtt://homepi.local')

const board = five.Board({
    io: new Raspi()
  }
);
const orange = "GPIO26"
const yellow = "GPIO19"
const pink = "GPIO13"
const blue = "GPIO6"
let speed = 6;

let stepper;

mqtt.on("connect",()=>{
  mqtt.subscribe("Boiler/setTemp")
  mqtt.publish("Boiler/ackn","motor ready")
})

mqtt.on('message',(topic,message)=>{
  
})
board.on('ready', async () => {
  stepper = new Stepper(orange, yellow, pink, blue, speed)
  await stepper.cw(50);
  await stepper.ccw(50);

  board.repl.inject({
   stp:stepper
  })
  function setSpeed(sp) {
    stepper.speed = sp;
  }


})

