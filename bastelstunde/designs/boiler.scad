include <toolbox_1.1.scad>
include <devices.scad>
use <axe.scad>
use <teardrop.scad>

print=true;
shrink=1.1;
thick=1.8;
$fn=100;
spacer=17;
motor_diam=29;
motor_gap_diam=10.5;
motor_mount_distance=30.8;
support_width=55;
support_height=35;
support_fixation=4;
photosensor_dim=[6.4,11.5,14];
photosensor_bracket=2.0;

boiler_side_axe=["single_cut",5.8,4.5];
boiler_side_rotation=60;
motor_side_axe=["double_cut",4.95,2.95];


// die L-Winkel
boiler_mount_hole=4;
boiler_mount_dist=46;
boiler_side_dist=27.09;
boiler_spacer=spacer-5;
boiler_screw_off=11.5;
carrier_size=6.5;

center=[support_width/2,support_height/2];
mount_off=center.x-boiler_mount_dist/2;
mount=motor_mount_distance/2+support_fixation/2;
 
if(print){
  adapter();
  translate([-25,20,0]) support();
  translate([-45,0,0])  carrier();
  translate([-45,20,0]) carrier();
  translate([45,0,0]) raspi_support();  
}else{
  adapter();
  translate([-support_width/2,-motor_gap_diam/2-support_width/2+3.5,-spacer/2]) 
  support();
  rotate([90,90,0])
    translate([-boiler_spacer,+mount_off+boiler_screw_off,-carrier_size/2])
  carrier();

}

module carrier(){
  l=carrier_size*1.5;
  w=carrier_size;
  difference(){
  union(){
    cube([2*thick,boiler_screw_off+w/2,l]);
    cube([boiler_spacer+2*thick,w,l]);
  }
   translate([boiler_spacer-2,w/2,l/2])
      teardrop(boiler_mount_hole/2,boiler_spacer+4,90);
    translate([2*thick,boiler_screw_off,l/2])
      teardrop(boiler_mount_hole/2,6*thick,90);
    //translate([-thick,w,center(l,boiler_mount_hole)])
    //cube([4*thick,boiler_screw_off-w/2-thick,boiler_mount_hole]);

  }
}


//Grundplatte
module support(){
    intersection(){
      support_1();
      translate([center.x,support_height+thick,0])
        cylinder(r=support_height, h=2*thick+photosensor_dim.z);
    }
    difference(){  
        intersection(){   
            support_2();
            translate([center.x,support_height-20,0]) 
                cylinder(r=support_height, h=2*thick+photosensor_dim.z);
      
    }
    // Schraublöcher untere Platte für Rapberry Träger
    translate([center.x-mount,2*center.y+2,-2*thick])
        cylinder(d=support_fixation,h=4*thick);
    translate([center.x+mount,2*center.y+2,-2*thick])
        cylinder(d=support_fixation,h=4*thick);
    translate([center.x,2*center.y+10,-2*thick])
        cylinder(d=support_fixation,h=4*thick);
    
    }
}

// Gehäuse für raspi zero
module raspi_support(){
    c=pizero_constants();
    l=lookup(total_length,c);
    w=lookup(total_width,c);
    zoff=thick+support_fixation-0.2;
    difference(){
        union(){
           raspberry_zero_base(thick=thick);
            translate([-2,-2,thick])
                roundedBox([l+4,w+8,15],radius=3,thick=thick);
            }
     
    rotate([0,0,90]){
        translate([w+5,-l/2,zoff])
            teardrop(support_fixation/2,10,90);
        translate([w+5,-l/2+mount,zoff+7])
            teardrop(support_fixation/2,10,90);
       translate([w+5,-l/2-mount,zoff+7])
            teardrop(support_fixation/2,10,90);
        rotate([90,0,90])
            translate([-l/2,25,w])
                cylinder(d=30,h=10);
    }
    raspi_zero_power(offset=[0,0,0.5],thick=thick);
    
    }
}

module support_2(){
    translate([0,support_height,0])
        cube([support_width,support_height,thick]);
}



module support_1(){
  ps=photosensor_dim;
  m=ps+[2*thick,2*thick,thick];
  m_off=[support_width/2-ps.x/2,5,thick];
  difference(){
    union(){
     cube([support_width,support_height,thick]); 
      // Lichtschranken-Halter 
     translate(m_off)
      box([ps.x,ps.y,photosensor_bracket-2],thick=thick);
     translate(m_off+[-thick,-thick,photosensor_bracket-0.5]) 
        cube([thick,4.5,ps.z-photosensor_bracket]);
     //translate(m_off-[thick+1-ps.y,2.2,0]) 
           //cube([thick,thick+2,ps.z]);
     translate(m_off+[ps.y-ps.y/2+0.7,-thick,photosensor_bracket-0.5]) 
      cube([thick,4.5,ps.z-photosensor_bracket]);
     //translate(m_off-[thick+2,2.2,0]) 
       //    cube([thick,thick+2,ps.z]);
        
      
    }
      translate([center.x,support_height-motor_gap_diam/2-3.5,-2*thick])
        cylinder(d=motor_gap_diam,h=thick*4);
    // Bohrlöcher
      translate([center.x-mount,center.y,-2*thick])
        cylinder(d=support_fixation,h=4*thick);
      translate([center.x+mount,center.y,-2*thick])
        cylinder(d=support_fixation,h=4*thick);
        
    
    // Lichtschranken-Aussparung im Boden
    translate([m_off.x,m_off.y-thick-.2,thick-1.5])
      cube([ps.x,ps.y+thick+.2,photosensor_bracket+thick+1]);
   
    // Schlitze für Boiler-Befestigung
    translate([thick,support_height-motor_gap_diam,-thick])
    cube([15,boiler_mount_hole,3*thick]);
    translate([support_width-15-thick,support_height-motor_gap_diam,-thick])
      cube([15,boiler_mount_hole,3*thick]);
   
    }
  
}


// Rad
module adapter(){
  difference(){
    union(){
      cylinder(d=30,h=2.5);
      cylinder(d=10,h=spacer);
    }
    translate([-5,-2,-5])
      rotate([0,0,90])
        cube([4,10,10]);
    translate([0,0,-1])
      axe(motor_side_axe,height=5,shrinkfactor=shrink);
    translate([0,0,spacer-5])
        rotate([0,0,boiler_side_rotation])
        axe(boiler_side_axe,height=8,shrinkfactor=shrink);
  }
}
