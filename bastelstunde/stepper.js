/*********************************************************************
 * Johnny Five driver for unipolar stepper motor (e.g. 28BYJ-48) 
 *********************************************************************/
const five = require('johnny-five')
const seq = ["0111", "0011", "1011", "1001", "1101", "1100", "1110", "0110"]; 
/**
 * Constructor arguments: 4 Pins for motor coils, delay in milliseconds to wait after
 * each step (allow the motor to reach new position).
 */
class Stepper {

  // eine umdrehung: 8*64 steps

  constructor(orange, yellow, pink, blue, delay) {
    this.in1 = new five.Pin(orange, { mode: five.Pin.OUTPUT });
    this.in2 = new five.Pin(yellow, { mode: five.Pin.OUTPUT });
    this.in3 = new five.Pin(pink, { mode: five.Pin.OUTPUT });
    this.in4 = new five.Pin(blue, { mode: five.Pin.OUTPUT });
    this.speed = delay;
  }

  set_step(step) {
    this.in1.write(parseInt(step[0]))
    this.in2.write(parseInt(step[1]))
    this.in3.write(parseInt(step[2]))
    this.in4.write(parseInt(step[3]))
  }

  /** 
  Set the scale for the "moveTo" function. Wrapper around five.map()
  @argument steps: Steps for 360° (defaults to 8*64)
  @argument start: assumed null position
  @argument end: maximum steps
  @argument domainFrom: Lower bound to map
  @argument domainTo: upper bound to map
  */
  setScale(steps, start, end, domainFrom, domainTo) {
    this.steps360 = steps;
    this.position = start;
    this.calc = val => {
      return five.Fn.map(val, start, end, domainFrom, domainTo)
    }
  }


  async ccw(steps) {
    for (let i = 0; i < steps; i++) {
      for (let j = seq.length - 1; j >= 0; j--) {
        let step = seq[j]
        this.set_step(step);
        await this.sleep(this.speed);
      }
    }
  }
  async cw(steps) {
    for (let i = 0; i < steps; i++) {
      for (let j = 0; j < seq.length; j++) {
        let step = seq[j]
        this.set_step(step);
        await this.sleep(this.speed);
      }
    }
  }

  sleep(ms) {
    return new Promise(resolve => {
      setTimeout(resolve, ms)
    })
  }
}


exports.Stepper = Stepper