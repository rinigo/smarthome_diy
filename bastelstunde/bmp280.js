
const five=require('johnny-five')
const Raspi=require('raspi-io');
const altitude = 440; // unsere Höhe in müM.

// MQTT einbinden und mit dem MQTT Broker auf dem Raspberry verbinden.
const mqtt=require('mqtt').connect('mqtt://localhost',{
  'cliendId': 'wohnzimmer-barometer',
  'will': { 
    'topic': 'info/connection/barometer',
    'payload': 'getrennt'
    
  }
})   

mqtt.on("connect",()=>{
  mqtt.publish("info/connection/barometer","verbunden");
});

const MQTT_TOPIC="/Wetter/Wohnzimmer/"

const board=new five.Board({
  io: new Raspi(),
  repl: false
})

// Korrektur auf aktuelle Höhe über Meeresspiegel, in mbar
const corr=(raw)=>{
  const corrected=raw/Math.pow((1-altitude/44330),5.255)
  return (10*corrected).toFixed(1)
}

board.on('ready',()=>{
	const bmp280=new five.Multi({
    controller: 'BME280',
    freq: 10000
	})
	bmp280.on('data',()=>{
		let temperature=bmp280.thermometer.celsius.toFixed(1);
		let pressure=corr(bmp280.barometer.pressure);
		let humidity=bmp280.hygrometer.relativeHumidity
		let meters=bmp280.altimeter.meters
    console.log(`${temperature}°C, ${pressure} kPa, ${humidity}%rH, ${+meters}m`);
    // Statt Loggen auf die Konsole schicken wir die Resultate zum MQTT Broker.
    mqtt.publish(MQTT_TOPIC+"Temperatur",temperature+" °C")
    mqtt.publish(MQTT_TOPIC+"Luftdruck",pressure+" mbar")
	})
})
