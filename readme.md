## Software zum Buch

Dies ist  die Quellcode-Referenz zum Buch '[Smarthome DIY](https://leanpub.com/smarthdiy)' von Roger Inigo, (c) 2018.                      

Sie dürfen dieses Programm für Anschauungszwecke verwenden und beliebig umgestalten. 

Sie tun dies absolut auf eigenes Risiko. Für korrekte Funktion oder Eignung für einen bestimmten Zweck kann keine Gewähr übernommen werden. 

Nutzung für kommerzielle Zwecke ist nicht gestattet.  
