import * as env from '../config/environment.json'
const gauge_size = 242;
const switch_size = 80;

const climate = {
  temperature: {
    caption: "Temperatur",
    suffix: "°C",
    minValue: 17,
    maxValue: 35,
    precision: 1
  },
  humidity: {
    caption: "Luftfeuchte",
    suffix: "%",
    minValue: 20,
    maxValue: 80,
    precision: 0
  },
  temp_scale: {
    bands: [{ from: 10, to: 17, color: "#8cf2e4" },
    { from: 15, to: 20, color: "yellow" },
    { from: 20, to: 26, color: "green" },
    { from: 26, to: 35, color: "red" }],
    minValue: 15,
    maxValue: 35,
    suffix: "°C",
    precision: 1
  },
  humid_scale: {
    bands: [{ from: 20, to: 40, color: "yellow" },
    { from: 40, to: 60, color: "green" },
    { from: 60, to: 80, color: "yellow" }],
    minValue: 20,
    maxValue: 80,
    suffix: "%",
    precision: 0
  }
}

export interface DeviceConfig {
  caption: string
  state_ids?: Array<string>
  messages: Array<string>
  height: number
  width?: number    // if omitted: equals height
  interval: number
  [specific: string]: any
}


export default {
  "SWITCH_ON": 1,
  "SWITCH_OFF": 0,
  "SWITCH_AUTO": 2,

  wohnzimmertemp_cfg: {
    state_ids: [env.devices.wohnzimmer_temp, env.devices.wohnzimmer_hygro],
    width: gauge_size,
    height: gauge_size,
    upper: climate.temp_scale,
    lower: climate.humid_scale,
    messages: ["wohnzimmer_temp", "wohnzimmer_hygro"],
    caption: "Wohnzimmer",
    timeout: 86400,
    visible: true,
    interval: 300,
  } as DeviceConfig,
  aussentemp_cfg: {
    state_ids: [env.devices.aussen_temp, env.devices.aussen_hygro],
    width: gauge_size,
    height: gauge_size,
    interval: 300,
    messages: ["aussen_temp", "aussen_hygro"],
    caption: "Aussen",
    timeout: 86400,
    visible: true,
    upper: {
      bands: [{ from: -15, to: 0, color: "#8cf2e4" },
      { from: 0, to: 10, color: "yellow" },
      { from: 10, to: 25, color: "green" },
      { from: 25, to: 40, color: "red" }],
      minValue: -15,
      maxValue: 40,
      suffix: "°C",
      precision: 1,
    },
    lower: {
      bands: [{ from: 20, to: 40, color: "yellow" },
      { from: 40, to: 60, color: "green" },
      { from: 60, to: 80, color: "yellow" }],
      minValue: 20,
      maxValue: 80,
      suffix: "%",
      precision: 0
    },
  } as DeviceConfig,
  pushbutton_cfg: {
    state_ids: [env.devices.treppenlicht_status],
    caption: "Aussentreppe",
    messages: ["treppenlicht"],
    width: switch_size,
    height: switch_size,
    interval: 10
  } as DeviceConfig,
  fernsehlicht_cfg: {
    state_ids: [env.devices.fernsehlicht_status, env.devices.fernsehlicht_modus],
    width: switch_size,
    height: switch_size,
    messages: ["fernsehlicht_status", "fernsehlicht_modus"],
    caption: "TV-Licht",
    interval: 30
  } as DeviceConfig,
  powermeter_cfg: {
    state_ids: [env.devices.energy_grid_flow],
    width: gauge_size,
    height: switch_size,
    minValue: -10000,
    maxValue: 10000,
    caption: "Netto Strom",
    messages: ["fronius_net"],
    suffix: "W",
    padding: 10,
    bands: [
      { from: -10000, to: 0, color: "red" },
      { from: 0, to: 10000, color: "green" }
    ],
    modify: [x => -x],
    interval: 60
  } as DeviceConfig,
  carswitch_cfg: {
    state_ids: [env.devices._car_loader_mode, env.devices._car_loader_state, env.devices._car_loader_power],
    messages: ["autolader_modus", "autolader_status", "autolader_strom"],
    caption: "Auto-Ladegerät",
    interval: 60,
    width: gauge_size,
    height: switch_size,
    modify: [x=>x,x=>x,(x: number) => x < 0 ? 0 : Math.round(3 * x)+"W"],
    positions: [
      "0:Immer aus",
      "S:Nur mit Sonnenenergie",
      "SN:Sonne und Niedertarif",
      "1:Immer an"
    ]
  } as DeviceConfig
}
