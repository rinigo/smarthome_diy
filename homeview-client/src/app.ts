/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018-2020                  *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { EventAggregator } from 'aurelia-event-aggregator';
import { autoinject } from 'aurelia-framework';
import * as env from '../config/environment.json'
import configs, { DeviceConfig } from './config'
import { IOBROKER_VALUE, FetchService } from './services/fetchservice'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './styles.scss'
const dev = env.devices

@autoinject
export class App {
  fetcher = new FetchService()
  conf = configs
  constructor(private ea: EventAggregator) {
    let gauges: Array<DeviceConfig> = [configs.aussentemp_cfg, configs.wohnzimmertemp_cfg, configs.powermeter_cfg]
    let switches: Array<DeviceConfig> = [configs.fernsehlicht_cfg, configs.carswitch_cfg, configs.pushbutton_cfg]
    this.subscribe(switches)

    gauges.concat(switches).forEach(device => {
      setInterval(() => {
        this.getValues(device)
      }, env.debug ? 60000 : device.interval * 1000)
      setTimeout(()=>this.getValues(device),500)
    })
  }

  /**
   * Subscribe to messages of all configured TriStateButtons
   * and send changed values to ioBroker
   * @param switches Array with TristateButton configurationa
   */
  subscribe(switches: Array<DeviceConfig>) {
    switches.forEach(sw => {
      for (let i = 0; i < sw.messages.length; i++) {
        console.log("subscribing " + sw.messages[i])
        this.ea.subscribe(sw.messages[i], msg => {
          console.log(sw.messages[i] + " -> " + msg)
          this.fetcher.setIoBrokerValue(sw.state_ids[i], msg).then(res => {
            console.log(res)
          })
        })
      }
    });
  }

  /**
   * Fetch ioBroker values for all configured Gauges
   * @param gauges Array with gauge configurations
   */
  getValues(device: DeviceConfig) {
    this.fetcher.getIobrokerValues(device.state_ids).then(results => {
      for (let i = 0; i < results.length; i++) {
        this.ea.publish(device.messages[i], results[i])
      }
    }, reason => {
      alert("an error occured " + reason)
    })
  }
}
