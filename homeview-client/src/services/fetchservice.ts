/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018-2020                  *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { HttpClient } from 'aurelia-fetch-client'
import * as env from '../../config/environment.json'

export type IOBROKER_VALUE = boolean | string | number
export interface IOBROKER_STATE {
  val: IOBROKER_VALUE
  ack: boolean
}

export class FetchService {
  private http = new HttpClient()

  /**
   * Eine JSON Antwort von einer URL holen, oder
   * einen zufälligen Mock-Wert, falls 
   * environment.mock== true ist, oder
   * die URL mit fake:// anfängt.
   * @param url die URL
   */
  public async fetchJson(url: string): Promise<Partial<IOBROKER_STATE>> {
    if (env.mock || url.startsWith("fake")) {
      let upper = 100
      let lower = -10
      KNOWN_SOURCES.find(source => {
        if (url.toLowerCase().includes(source.s)) {
          upper = source.u
          lower = source.l
          return true
        } else {
          return false
        }
      })
      const r = Math.random()
      const result= {
        val: Math.round((r *
          (upper - lower) + lower) * 10) / 10
      }
      return result
    } else {
      try {
        const raw = await this.http.fetch(url)
        return await raw.json()
      } catch (err) {
        console.log(err)
        return undefined
      }
    }
  }

  /* 
    Das .val Attribut der Antwort einer
    Anfrage liefern.
  */
  public async fetchJsonVal(url: string): Promise<IOBROKER_VALUE> {
    let result = await this.fetchJson(url)
    return result.val
  }

  /**
   * Einen ioBroker State auslesen
   * @param id die ID des States
   */
  public async getIobrokerValue(id: string): Promise<IOBROKER_VALUE> {
    const encoded = encodeURIComponent(id)
    try {
      return await this.fetchJsonVal(`${env.iobroker}/get/${encoded}`)
    } catch (err) {
      console.log(err)
      return undefined
    }
  }

  /**
   * Mehrere ioBroker States auslesen und zusammen zurückliefern
   * @param ids die IDs der gesünschten States
   */
  public async getIobrokerValues(ids: Array<string>): Promise<Array<IOBROKER_VALUE>> {
    const jobs = ids.map(id => {
      // const encoded = encodeURIComponent(id)
      return this.getIobrokerValue(id)
    })
    return await Promise.all(jobs)
  }

  public async setIoBrokerValue(id, value) {
    if (env.mock) {
      return {
        "id": id,
        "value": value
      }
    } else {
      const encoded = encodeURIComponent(id)
      try {
        let raw = await this.http.fetch(`${env.iobroker}/set/${encoded}?value=${value}`)
        let result = await raw.json();
        return result
      } catch (err) {
        console.log(err)
        return undefined
      }
    }
  }
}

const KNOWN_SOURCES = [
  { s: "temperatur", l: -1, u: 36 },
  { s: "humid", l: 20, u: 80 },
  { s: "energy", l: 1000, u: 100000 },
  { s: "power", l: 0, u: 10000 },
  { s: "012", l: 0, u: 2 },
  { s: "cent", l: 0, u: 100 },
  { s: "luftdruck", l: 960, u: 1050 },
  { s: "pressure", l: 960, u: 1050 }
]
