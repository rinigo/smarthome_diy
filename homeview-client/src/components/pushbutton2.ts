/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018-2020                  *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { autoinject, noView, bindable } from "aurelia-framework";
import { Component, eaMessage, Helper } from "./helper";
import { detect } from 'detect-browser'
import { selection } from 'd3-selection';
import { DeviceConfig } from './../config';


// Leider will Safari es anderes als die Anderen...   (1)
let LINK = "href"
const browser = detect()
if (browser && (browser.name === 'safari' || browser.name === 'ios')) {
  LINK = "xlink:href"
}

@autoinject
@noView
export class PushButton2 implements Component {
  @bindable cfg: DeviceConfig
  body: selection;
  component_name: "Push Button"
  private state: boolean
  private button;

  constructor(private hlp: Helper, public element: Element) { }
  attached() {
    this.hlp.initialize(this, {
      height: 110,
      messages: ["pushbutton2"],
      caption: "Klick mich",
      interval: 60
    })
  }

  configure() { }

  render() {
    const dim = this.hlp.defaultFrame(this)
    this.button = this.body.append("svg:image")
      .attr(LINK, "/off_button.png")
      .attr("x", dim.x)
      .attr("y", dim.y)
      .attr("width", dim.w)
      .attr("height", dim.h)
      .on("click", event => {     //(1)
        this.hlp.ea.publish(this.cfg.messages[0], this.state ? false : true)
      })

  }

  update(val: eaMessage) {
    const img = val.data ? "/on_button.png" : "/off_button.png"
    this.state = val.data
    this.button.attr(LINK, img)
  }


}
