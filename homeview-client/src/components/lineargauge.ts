/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018-2020                  *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import {autoinject, bindable, noView} from 'aurelia-framework';
import {scaleLinear} from "d3-scale";
import 'd3-transition'
import {Helper, Component, eaMessage} from './helper'
import { selection } from 'd3-selection';
import { DeviceConfig } from './../config';


@autoinject
@noView
export class Lineargauge implements Component{
  @bindable cfg: DeviceConfig
  readonly component_name = "Lineargauge"
  private scale: scaleLinear
  body: selection
  private indicator
  private value

  /**
   *  Der Constructor wird eigentlich nur benötigt, um die injizierten Objekte aufzunehmen.
   */
  constructor(public element: Element, private hlp: Helper) {
  }

  attached() {
    this.hlp.initialize(this,{
      messages: ["Lineargauge_value"],
      caption: "",
      suffix: "",
      minValue    : 0,
      maxValue    : 100,
      height : 50,
      width  : 180,
      padding: 0,
      bands  : [{from: 0, to: 100, color: "blue"}],
    })
  }

  configure() {
    this.scale = scaleLinear()
      .domain([this.cfg.minValue, this.cfg.maxValue])
      .range([5 + this.cfg.padding, this.cfg.width - 5 - this.cfg.padding])
      .clamp(true)
  }

  render() {
    // Rahmen
    const dim=this.hlp.defaultFrame(this)
    const baseline = dim.y+4*dim.h/5
    const scalesize=dim.h/10
    // draw colored bands
    this.cfg.bands.forEach(band => {
      this.hlp.line(this.body, this.scale(band.from), baseline, this.scale(band.to), baseline, band.color, scalesize).attr("opacity", 0.5)
    })

    // draw tick marks and text on every second tick
    const ticks = this.scale.ticks(10)
    const tickFormat = this.scale.tickFormat(10, "s")
    const fontSize = dim.h / 6
    let even = true
    ticks.forEach(tick => {
      const pos = this.scale(tick)
      this.hlp.line(this.body, pos, baseline + scalesize/2, pos, baseline - scalesize, "black", 0.6)
      if (even || (tick == 0)) {
        this.hlp.stringElem(this.body, pos, baseline-1.5 * fontSize, fontSize, "middle")
          .text(tickFormat(tick))
      }
      even = !even
    })

    // Zeiger
    this.indicator = this.hlp.line(this.body, this.scale(0), dim.y+1, this.scale(0), dim.y+dim.h-2, "red", 1.2)
      .attr("id", "indicator1")


    // Textanzeige des Messwerts
    let valueFontSize = 0.8 * 3*dim.h/5
    this.value = this.hlp.stringElem(this.body,
      this.scale(this.cfg.minValue + ((this.cfg.maxValue - this.cfg.minValue) / 2)),
      valueFontSize-3,
      valueFontSize,
      "middle",
      0
    ).attr("opacity", 0.4).style("fill", "grey")

  }


  update(newVal:eaMessage) {
    const value=newVal.data
    const x = this.scale(value)
    this.indicator.transition()
      .duration(300)
      .attr("x1", x)
      .attr("x2", x)
    this.value.text(value + this.cfg.suffix)
  }
}
