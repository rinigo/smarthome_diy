/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018-2020                  *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { bindable, noView, autoinject } from 'aurelia-framework'
import { EventAggregator, configure } from "aurelia-event-aggregator"
import { Helper, Component, eaMessage } from './helper'
import { selection } from 'd3-selection';
import { DeviceConfig } from './../config';

/**
 * Die Nachricht, die der TristateButton über den EventAggregator versendet und empfängt
 */
export interface tristateMessage {
  direction: "in" | "out"
  state: "on" | "off"
  mode: "auto" | "manual"
  power: number
}


/**
 * Ein TristateButton ist ein Knopf, der auf "ein", "aus" oder "automatik" geschaltet werden kann.
 */
@autoinject
@noView
export class TristateButton implements Component {
  @bindable cfg: DeviceConfig
  component_name = "TristateButton"
  body: selection
  private actState: boolean
  private actMode: boolean
  private upper;
  private lower;
  private autoText;
  private powerText;

  constructor(private hlp: Helper, public element: Element,
    private ea: EventAggregator) { }

  configure() { }

  attached() {
    this.hlp.initialize(this, {
      height: 110,
      caption: "Tristate",
      messages: [],
      interval: 10000
    })

  }

  render() {
    const dim = this.hlp.defaultFrame(this)
    const ratio = 0.6

    this.upper = this.hlp.rectangle(this.body, dim.x,
      dim.y, dim.w, dim.h, "light_on")
      .attr("rx", 15)
      .attr("ry", 15)
      .attr("clip-path", "url(#clip-bottom)")
      .on("click", () => {
        this.ea.publish(this.cfg.messages[0], this.actState ? false : true)
        if (this.actMode) {
          this.ea.publish(this.cfg.messages[1], false)
        }
      })

    this.lower = this.hlp.rectangle(this.body, dim.x,
      dim.y, dim.w, dim.h, "mode_auto")
      .attr("rx", 15)
      .attr("ry", 15)
      .attr("clip-path", "url(#clip-top)")
      .on("click", () => {
        this.ea.publish(this.cfg.messages[1], this.actMode ? false : true)
      })

    const defs = this.body.append("svg:defs")
    defs.append("svg:clipPath")
      .attr("id", "clip-bottom")
      .append("svg:rect")
      .attr("x", dim.x)
      .attr("y", dim.y)
      .attr("width", dim.w)
      .attr("height", dim.h * ratio)

    defs.append("svg:clipPath")
      .attr("id", "clip-top")
      .append("svg:rect")
      .attr("x", dim.x)
      .attr("y", dim.y + dim.h * ratio)
      .attr("width", dim.w)
      .attr("height", dim.h * (1 - ratio))

    let fontsize = Math.floor(dim.h / 5)
    let text_cx = this.cfg.height / 2
    let text_y = Math.round(dim.y + dim.h * ratio + (dim.h * (1 - ratio)) / 2) - 1
    let txtgroup = this.body.append("svg:g")
      .attr("transform", `translate(${text_cx},${text_y})`)
    this.autoText = this.hlp.stringElem(txtgroup, 0, 0, fontsize, "middle")
      .text("auto")
      .on("click", () => {
        this.ea.publish(this.cfg.messages[1], this.actMode ? false : true)
      })

    this.powerText = this.hlp.stringElem(this.body, text_cx, dim.h / 2, fontsize, "middle", 0)
      .style("pointer-events", "none");

  }

  update(val: eaMessage) {
    const msg = val.message
    const poss = this.cfg.messages.length
    if (poss > 0) {
      if (msg == this.cfg.messages[0]) {  // Status
        this.actState = val.data
        this.upper
          .classed("light_on", val.data)
          .classed("light_off", !val.data)
      } else if (poss > 1 && msg == this.cfg.messages[1]) {
        this.actMode = val.data
        this.lower
          .classed("mode_manual", !val.data)
          .classed("mode_auto", val.data)
        this.autoText.attr("opacity", val.data ? 1.0 : 0.2)
      } else if (poss > 2 && msg == this.cfg.messages[3]) {
        let power = val.data
        if (power > 1000) {
          power = Math.round(power)
        } else if (power > 100) {
          power = Math.round(10 * power) / 10
        } else {
          power = Math.round(100 * power) / 100
        }
        this.powerText.text(power + " W")
      }
    }

  }

}
