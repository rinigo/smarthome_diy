/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018-2020                  *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { bindable, noView, autoinject } from 'aurelia-framework'
import { Helper, Component, eaMessage } from './helper'
import { selection, scaleOrdinal } from 'd3'
import { DeviceConfig } from './../config';

@noView
@autoinject
export class SlideSwitch implements Component {
  @bindable cfg: DeviceConfig
  body: selection
  readonly component_name = "SlideSwitch"
  private block: selection
  private indicator: selection
  private scale: scaleOrdinal
  private posText: selection
  private posTextContent: string
  private actPower: string

  constructor(private hlp: Helper, public element: Element) { }

  attached() {
    // default configuration. Will be overriden usually
    this.hlp.initialize(this, {
      messages: ["slideswitch"],
      caption: "Schalter",
      positions: ["0:off", "1:on"],
      width: 200,
      height: 50,
      interval: 60
    })
  }
  configure() {
    const elemsize = (this.cfg.width - 2 * Helper.BORDER) / this.cfg.positions.length
    const r = []
    for (let i = 0; i < this.cfg.positions.length; i++) {
      r.push(i * elemsize)
    }
    this.scale = scaleOrdinal()
      .domain(this.cfg.positions.map(p => p.split(/:/)[0]))
      .range(r)
  }

  render() {
    const dim = this.hlp.defaultFrame(this)
    const fields = this.cfg.positions.length
    const fieldsize = dim.w / fields
    const valueFontSize = 0.8 * 3 * dim.h / 5
    const headerFontSize = dim.h / 4 * 0.9
    this.posText = this.hlp.stringElem(this.body, dim.x + (dim.w - dim.x) / 2, dim.h + headerFontSize / 3, headerFontSize, "middle")

    for (let i = 0; i < fields; i++) {
      const offs = i * fieldsize
      this.hlp.rectangle(this.body, dim.x + offs, dim.y, fieldsize, 3 * dim.h / 4, "inner")
        .attr("opacity", 0.1)
        .on("click", () => {
          this.hlp.ea.publish(this.cfg.messages[0], this.cfg.positions[i])
        })

      this.hlp.stringElem(this.body, i * fieldsize + fieldsize / 2, dim.y + (dim.h - dim.y) / 2.2, valueFontSize, "middle")
        .attr("opacity", 0.8).style("fill", "blue")
        .text(this.cfg.positions[i].split(/:/)[0])
        .on("click", () => {
          this.hlp.ea.publish(this.cfg.messages[0], this.cfg.positions[i])
        })
    }
    const bl = { x: dim.x + 2, y: dim.y + 2, w: fieldsize - 4, h: (3 * dim.h / 4) - 4 }
    this.block = this.body.append("g")
    this.indicator = this.hlp.rectangle(this.block, bl.x, bl.y, bl.w, bl.h, "slider_frame_on")
      .attr("rx", "4px")
    this.hlp.rectangle(this.block, bl.x + 4, bl.y + 4, bl.w - 8, bl.h - 8, "slider_center")
      .attr("rx", "4px")

    this.update({ data: this.cfg.positions[0], message: this.cfg.messages[0] })

  }

  setPosText(ntext?: string) {
    let strs = (ntext || this.posTextContent).split(":")
    let pos = strs[0]
    if (strs.length < 2) {
      ntext = this.cfg.positions.find(e => e.startsWith(pos + ":"))
      strs = ntext.split(":")
    }
    this.posTextContent = pos + ":" + strs[1]
    if (this.actPower) {
      strs[1] += ": " + this.actPower
    }
    this.posText.text(strs[1])
    return pos
  }

  update(data: eaMessage) {
    const msg = data.message
    if (msg === this.cfg.messages[0]) {
      if (data.data) {
        const pos = this.setPosText(data.data)
        this.block.transition()
          .duration(300)
          .attr("transform", `translate(${this.scale(pos)},0)`)
      }
    } else if (this.cfg.messages.length > 1 && msg == this.cfg.messages[1]) {
      this.indicator.classed("slider_frame_on", data.data)
      this.indicator.classed("slider_frame_off", !data.data)
    } else if (this.cfg.messages.length > 2 && msg == this.cfg.messages[2]) {
      this.actPower = data.data
      this.setPosText()
    }
  }

}
