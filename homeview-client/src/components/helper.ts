/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018-2020                  *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

import { arc } from 'd3-shape'
import { select, selection } from 'd3-selection'
import { DeviceConfig } from './../config';
import { EventAggregator } from 'aurelia-event-aggregator'
import { autoinject } from 'aurelia-framework';
export interface Component {
  configure()
  render()
  update(data: eaMessage)
  cfg: DeviceConfig
  element: Element
  body: selection
  component_name: String
}

export type eaMessage = {
  message: string,
  data: any
}

export type N_StateMessage = {
  modes: Array<string>;
  direction: "in" | "out";
  state: boolean;
  power?: number;
  [propName: string]: any
}

@autoinject
export class Helper {
  static BORDER = 5

  constructor(public ea: EventAggregator) { }

  initialize(component: Component, defaultCfg: Partial<DeviceConfig>) {
    component.cfg = Object.assign(
      {
        modify: [a => a]
      }, defaultCfg, component.cfg)
    component.configure()
    component.body = select(component.element).append("svg:svg")
      .attr("class", component.component_name)
      .attr("width", component.cfg.width)
      .attr("height", component.cfg.height)
    component.render();
    for (let i = 0; i < component.cfg.messages.length; i++) {
      this.ea.subscribe(component.cfg.messages[i], data => {
        component.update(<eaMessage>{
          message: component.cfg.messages[i],
          data: component.cfg.modify[i](data)
        })
      })
    }
  }

  // helper to draw a rectangle
  rectangle(parent: selection, x: number, y: number, w: number, h: number, clazz = "inner") {
    return parent.append("svg:rect")
      .attr("x", x + "px")
      .attr("y", y + "px")
      .attr("width", w + "px")
      .attr("height", h + "px")
      .classed(clazz, true)
  }

  // helper to draw and position an arch
  arch(parent: selection, x: number, y: number, inner: number, outer: number, start: number, end: number, color: string, rotation: number) {
    let gen = arc()
      .startAngle(start)
      .endAngle(end)
      .innerRadius(inner)
      .outerRadius(outer)
    parent.append("svg:path")
      .style("fill", color)
      .attr("d", gen)
      .attr("transform", () => {
        return `translate(${x},${y}) rotate(${rotation})`
      })
  }


  /**
   *  helper to append a text element
  */
  stringElem(parent: selection, x: number, y: number, size: number,
    align: "start" | "middle" | "end" | "inherit", dy = undefined) {
    return parent.append("svg:text")
      .attr("x", x + "px")
      .attr("y", y + "px")
      .attr("text-anchor", align)
      .attr("dy", (dy || size / 2) + "px")
      .style("font-size", size + "px")
      .style("fill", "black")
  }

  // helper to add a line
  line(parent, x1: number, y1: number, x2: number, y2: number, color: string, width: number) {
    return parent.append("svg:line")
      .attr("x1", x1)
      .attr("x2", x2)
      .attr("y1", y1)
      .attr("y2", y2)
      .attr("stroke", color)
      .attr("stroke-width", width)
  }

  // helper to convert degrees into radiants
  deg2rad(deg: number): number {
    return deg * Math.PI / 180
  }

  defaultFrame(c: Component): { x: number, y: number, w: number, h: number } {
    const yoff = this.frame(c.body, c.cfg.width, c.cfg.height, c.cfg.caption, c.cfg.capsize)
    return {
      x: Helper.BORDER,
      y: yoff,
      w: c.cfg.width - 2 * Helper.BORDER,
      h: c.cfg.height - yoff - Helper.BORDER
    }
  }

  frame(parent, outer_width: number, outer_height: number, caption: string = undefined, capsize: number = outer_height / 8): number {
    this.rectangle(parent, 0, 0, outer_width, outer_height, "frame")
    let x_offset = Helper.BORDER
    let y_offset = Helper.BORDER
    let width = outer_width - 2 * Helper.BORDER
    let height = outer_height - 2 * Helper.BORDER
    if (caption) {
      let fontsize = Math.round(capsize)
      y_offset = y_offset + fontsize
      height = height - fontsize - 2
      this.rectangle(parent, x_offset, y_offset, width, height, "inner")
      let off = (y_offset - fontsize) / 2
      this.stringElem(parent, outer_width / 2, Helper.BORDER + off, fontsize, "middle").text(caption)
      return y_offset
    } else {
      this.rectangle(parent, x_offset, y_offset, width, height, "inner")
      return Helper.BORDER
    }
  }
}
