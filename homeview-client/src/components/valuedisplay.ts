import {bindable} from 'aurelia-framework'

export class ValueDisplay{
  @bindable value;
  @bindable prefix;
  @bindable suffix;
}
