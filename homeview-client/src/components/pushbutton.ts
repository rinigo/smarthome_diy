import { DeviceConfig } from './../config';
import { bindable, autoinject } from 'aurelia-framework'
import { EventAggregator } from 'aurelia-event-aggregator'

@autoinject
export class PushButton {
  @bindable cfg: DeviceConfig
  @bindable pressed: boolean;

  constructor(private ea: EventAggregator) { }

  attached() {
    this.cfg = Object.assign({}, {
      message: "pushbutton"
    }, this.cfg)
  }

  toggle() {
    this.pressed = !this.pressed
    this.ea.publish(this.cfg.messages[0], this.pressed)
  }
}
