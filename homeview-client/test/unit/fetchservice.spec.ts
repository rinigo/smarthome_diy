import { FetchService } from '../../src/services/fetchservice'

describe("FetchService", () => {
  let fetcher
  beforeEach(() => {
    fetcher = new FetchService()
  })
  it("fetches JSON objects from remote servers", (done) => {
    fetcher.fetchJson("fake://energy").then(result => {
      expect(result).toBeDefined()
      expect(result['val']).toBeDefined()
      done()
    })
  })
  it("gives temperature between -1 and 36", async () => {
    const result = await fetcher.fetchJsonVal("fake://temperatur")
    expect(result).toBeGreaterThan(-1)
    expect(result).toBeLessThan(36)
  })
  it("gives humidity between 20 and 80 %", (done) => {
    fetcher.fetchJsonVal("fake://humidity").then(result => {
      expect(result).toBeGreaterThan(20)
      expect(result).toBeLessThan(80)
      done()
    })
  })
})
