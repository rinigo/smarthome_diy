/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

var latitude=46.631094047;
var longitude=7.72370708; 
var API_KEY="Sollte geheim bleiben"; 

const request=require('request')
const moment=require('moment')

const NOW = "wetter.darksky.jetzt."
const TODAY = "wetter.darksky.heute."
const TOMORROW = "wetter.darksky.morgen."

const attribution = "https://darksky.net/poweredby/"
createState("wetter.darksky.lastcall","")
createState(NOW + "temp", 0)
createState(NOW + "bedeckt", 0)
createState(NOW + "wind", 0)
createState(NOW + "niederschlag", 0)


createState(TODAY + "maxtemp", 0)
createState(TODAY + "mintemp", 0)
createState(TODAY + "bedeckt", 0)
createState(TODAY + "wind", 0)
createState(TODAY + "niederschlag", 0)

createState(TOMORROW + "maxtemp", 0)
createState(TOMORROW + "mintemp", 0)
createState(TOMORROW + "bedeckt", 0)
createState(TOMORROW + "wind", 0)
createState(TOMORROW + "niederschlag", 0)


var call = "https://api.darksky.net/forecast/" + API_KEY + "/" + latitude + "," + longitude + "?units=si&lang=de&exclude=minutely,daily,flags,alerts"

console.log("powered by: " + attribution)

const getMinMax = function (range, curr, accum) {
  const currtime = moment(curr.time * 1000)
  if (currtime.isAfter(range[0]) && currtime.isBefore(range[1])) {
    accum.minTemp = Math.min(accum.minTemp, curr.temperature)
    accum.maxTemp = Math.max(accum.maxTemp, curr.temperature)
    accum.wind = Math.max(accum.wind, curr.windSpeed)
    accum.cloudsum = accum.cloudsum + curr.cloudCover
    accum.precipsum = accum.precipsum + curr.precipIntensity * curr.precipProbability
    accum.counter = accum.counter + 1
    accum.cloud = accum.cloudsum / accum.counter
    accum.precip = accum.precipsum / accum.counter
  }
  return accum
}

function fetch() {
  request(call, function (error, response, body) {
    if (error) {
      log.warning("Error! " + error)
    } else {
      if (response && (response.statusCode == 200)) {
        const forecast = JSON.parse(body)
        const today = forecast.hourly.data
        const now = forecast.currently

        setState(NOW + "temp", now.temperature)
        setState(NOW + "bedeckt", Math.round(100 * now.cloudCover))
        setState(NOW + "wind", now.windSpeed)
        setState(NOW + "niederschlag", now.precipIntensity)

        const tsNow = moment(now.time * 1000)
        const tomorrow = tsNow.clone()
        tomorrow.add(1, 'days')
        const spanToday = [tsNow.clone(), tsNow.clone().endOf('day')]
        const spanTomorrow = [tomorrow.clone().startOf("day"), tomorrow.clone().endOf("day")]
        const accumTemplate = {
          minTemp: 100, maxTemp: -100, wind: -5, cloudsum: 0, counter: 0, precipsum: 0
        }

        const todayMinMax = today.reduce((accum, curr) => getMinMax(spanToday, curr, accum)
          , Object.assign({}, accumTemplate))
        const tomorrowMinMax = today.reduce((accum, curr) => getMinMax(spanTomorrow, curr, accum), Object.assign({}, accumTemplate))
        setState(TODAY + "mintemp", todayMinMax.minTemp)
        setState(TODAY + "maxtemp", todayMinMax.maxTemp)
        setState(TODAY + "wind", todayMinMax.wind)
        setState(TODAY + "bedeckt", Math.round(100 * todayMinMax.cloud))
        setState(TODAY + "niederschlag", Math.round(100 * todayMinMax.precipsum))
        setState(TOMORROW + "mintemp", tomorrowMinMax.minTemp)
        setState(TOMORROW + "maxtemp", tomorrowMinMax.maxTemp)
        setState(TOMORROW + "wind", tomorrowMinMax.wind)
        setState(TOMORROW + "bedeckt", Math.round(100 * tomorrowMinMax.cloud))
        setState(TOMORROW + "niederschlag", Math.round(100 * tomorrowMinMax.precipsum))
        setState("wetter.darksky.lastcall",tsNow.toString())
      } else {
        console.log("no response")
      }
    }
  })
}

schedule("20 4,8,11,14,17,20,23 * * *",fetch)
