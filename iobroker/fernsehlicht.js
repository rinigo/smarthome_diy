/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

const hue="hue.0.Philips_hue.Wohnzimmer.on";

createState("fernsehlicht_manuell",AUTO)

function licht(val){
    if(getState("fernsehlicht_manuell").val==AUTO){
        if(getState("hue.0.Philips_hue.Wohnzimmer.on").val!=val){
            console.log("schalte Licht: "+val,'debug')
            setState(hue,val)
        }
    }
}

on({id: "javascript.0.fernsehlicht_manuell", val:OFF},function(){
    setState(hue,false)
})

on({id: "javascript.0.fernsehlicht_manuell", val:ON},function(){
    setState(hue,true)
})

schedule({astro: "sunset", shift: -15}, function () {
    log("sunset",'info')
    if(getState("lgtv.0.on"/*TV is ON*/).val){
        licht(true)
    }
});

on({id: 'lgtv.0.on', val: true, change: "ne"}, function(){
    log("tv switched on",'info')
    if(!isAstroDay()){
        licht(true)
    }
})

on({id: 'lgtv.0.on', val: false, change: "ne"}, function(){
    log("tv switched off",'info')
    licht(false)
})

schedule({astro: "sunrise"}, function(){
    log("sunrise")
    licht(false)
})
 