/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

createState("aussenlicht_manuell",AUTO)

const lichter="lightify.0.904AA200AA3EB07C.on"

function toggle(mode){
    log("toggle "+mode)
    if(getState("aussenlicht_manuell").val==AUTO){
        setState(lichter,mode)
    }
}


on({id: "javascript.0.aussenlicht_manuell", val:OFF}, function(){
    log("manuell aus")
    setState(lichter,false)
})
on({id: "javascript.0.aussenlicht_manuell",val: ON},function(){
    log("manuell an")
    setState(lichter,true)
})
on({id: "javascript.0.aussenlicht_manuell", val:AUTO},function(){
    log("switched to auto")
    if(isAstroDay()){
        log("it's day")
        setState(lichter,false)
    }else{
        log("it's night")
        setState(lichter,compareTime('sunset','23:59','between'))
    }
})

schedule({astro: "sunset", shift: 15}, function(){
    log("sunset, lights on")
    toggle(true)
})

schedule("30 23 * * *", function(){
    log("23:30, lights off")
    toggle(false)
})
