/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

const DEFAULT_BRI=35;

createState("aussenlicht_manuell",AUTO)

const treppenlicht="lightify.0.904AA200AA3EB07C.on"
const treppenlicht_bri="lightify.0.904AA200AA3EB07C.bri"
const tuerlicht="lightify.0.64EADA0000261884.on"
const sensor="hm-rpc.0.NEQ0320745.1.MOTION"
const helligkeit="hm-rpc.0.NEQ0320745.1.BRIGHTNESS"

/*
 Bedingtes schalten, nur im AUTO Modus
 */
function toggle(mode){
    log("toggle "+mode)
    if(getState("aussenlicht_manuell").val==AUTO){
        setState(treppenlicht,mode)
    }
}
/*
  manuelles Ausschalten
*/

on({id: "javascript.0.aussenlicht_manuell", val:OFF}, function(){
    log("manuell aus")
    setState(treppenlicht_bri,DEFAULT_BRI)
    setState(treppenlicht,false)
    setState(tuerlicht,false)
})

/*
Manuelles Einschalten
*/
on({id: "javascript.0.aussenlicht_manuell",val: ON},function(){
    log("manuell an")
    setState(treppenlicht,true)
    setState(treppenlicht_bri,90)
    setState(tuerlicht,true)
})

/*
  Auf Automatik schalten. Wenn es Tag ist: Alles aus, wenn es Nacht ist: Treppenlicht gedämpft an.
*/
on({id: "javascript.0.aussenlicht_manuell", val:AUTO},function(){
    log("switched to auto")
    setState(treppenlicht_bri,DEFAULT_BRI)
    setState(tuerlicht,false)
    if(isAstroDay()){
        log("it's day")
        setState(treppenlicht,false)
    }else{
        log("it's night")
        setState(treppenlicht,compareTime('sunset','23:59','between'))
    }
})

/*
  Jemand ist in den Erfassungsbereich des Sensors getreten. Falls es dunkel ist, wird das Licht
  für 2 Minuten eingeschaltet.
*/
on({id: sensor, val: true},function(){
    if(getState(helligkeit).val<90){
        setTimeout(function(){
            log("motion sensor timeout")
            setState("javascript.0.aussenlicht_manuell",AUTO)
        },120000)
        log("motion sensor activated")
        setState(tuerlicht,true)
        setState(treppenlicht, true)
        setState(treppenlicht_bri,100)
    }
})

/*
15 Minuten nach Sonnenuntergang Lichter einschalten (falls auf Automatik)
 */
schedule({astro: "sunset", shift: 15}, function(){
    log("sunset, lights on")
    toggle(true)
})

/*
  23:30 Uhr Lichter ausschalten (falls auf Automatik)
*/
schedule("30 23 * * *", function(){
    log("23:30, lights off")
    toggle(false)
})
