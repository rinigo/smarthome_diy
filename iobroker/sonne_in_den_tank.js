/*******************************************************************
 * Diese Datei ist Teil der Quellcodereferenz zum Buch             *
 * 'Smarthome DIY' von Roger Inigo, (c) 2018                       *
 * Sie dürfen dieses Programm für Anschauungszwecke verwenden      *
 * und beliebig umgestalten. Sie tun dies absolut auf              *
 * eigenes Risiko. Für korrekte Funktion oder Eignung              *  
 * für einen bestimmten Zweck kann keine Gewähr übernommen werden. *
 * Nutzung für kommerzielle Zwecke ist nicht gestattet.            *
 *******************************************************************/

const power_pv="fronius.0.powerflow.P_PV" // was wir produzieren
const power_use="fronius.0.powerflow.P_Grid" // was wir vom/zum Netz beziehen/liefern
const mystrom_switch="mystrom-wifi-switch.1.switchState" // Der Schalter

createState("loadcar_manual",AUTO)

function toggle(mode){
    log("auto "+mode+" requested")
    if(getState("javascript.0.loadcar_manual").val==AUTO){
        if(getState(mystrom_switch).val != mode){
            log("toggle "+mode, 'info')
            setState(mystrom_switch,mode)
        }
    }
}

on({id: "javascript.0.loadcar_manual",val: ON},function(){
    log("manual on")
    setState(mystrom_switch,true)
})

on({id: "javascript.0.loadcar_manual",val: OFF},function(){
    log("manual off")
    setState(mystrom_switch,false)
})

schedule("*/10 7-19 * * *",function(){
    var net_flow=getState(power_use).val
    log("available: "+getState(power_pv).val+", net flow: "+net_flow,'info')
    if( net_flow < -1500){
        toggle(true)
    }else if(net_flow > 0){
        toggle(false)
    }
})

schedule("5 20 * * *", function(){
    log("night schedule: on")
    toggle(true)
    
})

schedule("59 6 * * *", function(){
    log("day schedule: off")
    toggle(false)
})
